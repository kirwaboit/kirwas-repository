package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       //connecting to the buttons

        Button greyb =(Button) findViewById(R.id.greyb);
        Button blueb =(Button) findViewById(R.id.blueb);
        Button greenb =(Button) findViewById(R.id.greenb);
        Button redb =(Button) findViewById(R.id.redb);
        Button yellowb =(Button) findViewById(R.id.yellowb);



        //Setting the listeners to the buttons and making them pass us to the next screen
        greyb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int1= new Intent(MainActivity.this,grey.class);
                startActivity(int1);
            }
        });

        blueb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int2 = new Intent(MainActivity.this,blue.class);
                startActivity(int2);

            }

            });

        greenb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int3 = new Intent(MainActivity.this,green.class);
                startActivity(int3);

            }

        });


        redb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int4 = new Intent(MainActivity.this,red.class);
                startActivity(int4);

            }

        });

        yellowb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent int5 = new Intent(MainActivity.this,yellow.class);
                startActivity(int5);

            }

        });


    }


}
