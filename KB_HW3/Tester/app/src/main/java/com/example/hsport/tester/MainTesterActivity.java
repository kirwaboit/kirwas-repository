package com.example.hsport.tester;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.os.Handler;
import android.os.Message;


import java.lang.Math;



public class MainTesterActivity extends AppCompatActivity {

    int rem ;
    int firstPlayer ;
    int ranP1pick ;
    int ranP1speed ;
    int totalSpeed ;
    int totalP1keys ;
    int ranP2pick ;
    int ranP2speed ;
    int totalP2keys ;



    Handler myhandler1 = new Handler(){
        @Override
        public void handleMessage(Message msg) {

            ProgressBar p1progressBar = (ProgressBar) findViewById(R.id.p1progressBar);
            ProgressBar totalkeyprogressBar = (ProgressBar) findViewById(R.id.totalkeyprogressBar);
            p1progressBar.setProgress(totalP1keys);
            totalkeyprogressBar.setProgress(totalP1keys+totalP2keys);
            ProgressBar keyremprogressBar3 = (ProgressBar) findViewById(R.id.keyremprogressBar3);
            keyremprogressBar3.setProgress(7-ranP1pick );
        }
    };

    Handler myhandler2 = new Handler(){
        @Override
        public void handleMessage(Message msg) {

            ProgressBar p2progressBar = (ProgressBar) findViewById(R.id.p2progressBar);
            p2progressBar.setProgress(totalP2keys);
            ProgressBar totalkeyprogressBar = (ProgressBar) findViewById(R.id.totalkeyprogressBar);
            totalkeyprogressBar.setProgress(totalP1keys+totalP2keys);
            ProgressBar keyremprogressBar3 = (ProgressBar) findViewById(R.id.keyremprogressBar3);
            keyremprogressBar3.setProgress(7-ranP2pick );

        }
    };
    Handler myhandler3 = new Handler(){
        @Override
        public void handleMessage(Message msg) {

            TextView playWinnr = (TextView) findViewById(R.id.playWinnr);
            playWinnr.setText("PLAYER 1 WINS!!!!!!");
            TextView clktextview = (TextView) findViewById(R.id.clktextview);
            clktextview.setText("Play Time is "+totalSpeed + " seconds," );

        }
    };
    Handler myhandler4 = new Handler(){
        @Override
        public void handleMessage(Message msg) {

            TextView playWinnr = (TextView) findViewById(R.id.playWinnr);
            TextView clktextview = (TextView) findViewById(R.id.clktextview);
            clktextview.setText("Play Time is "+totalSpeed + " seconds," );
            playWinnr.setText("PLAYER 2 WINS!!!!!!");

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tester);

    }



  //this button click starts the app
     public void strtMyapp(View view) {

       Runnable myappRunnable = new Runnable() {
           @Override
           public void run() {
               //caclulation code in here
                rem = 7;
               firstPlayer = 0;
                ranP1pick = 0;
               ranP1speed = 0;
                totalSpeed = 0;
                totalP1keys = 0;
                ranP2pick = 0;
                ranP2speed = 0;
                totalP2keys = 0;
               // int nextPlayerTurn =0;

               firstPlayer = (int) (Math.random() * 2 + 1);

               if (firstPlayer == 1) {
                   System.out.println("PLAYER 1 GETs TO START");
               } else {
                   System.out.println("PLAYER 2 GETs TO START");
               }


               do {


                   if (firstPlayer == 1 && totalSpeed < 90 && totalP1keys < 50) {


                       System.out.println("PLAYER 1 TURN");

                       ranP1pick = (int) (Math.random() * rem + 1);


                       for (int x = 0; x < ranP1pick; x = x + 1) {

                           ranP1speed = (int) (Math.random() * rem + 1);

                           totalSpeed = totalSpeed + ranP1speed;
                           totalP1keys = totalP1keys + ranP1pick;
                           firstPlayer = 2;

                           myhandler1.sendEmptyMessage(0);

                       }




                   } else if (firstPlayer == 2 && totalSpeed < 90 && totalP2keys < 50) {

                       System.out.println("PLAYER 2 TURN");

                       ranP2pick = (int) (Math.random() * rem + 1);


                       for (int x = 0; x < ranP2pick; x = x + 1) {

                           ranP2speed = (int) (Math.random() * rem + 1);

                           totalSpeed = totalSpeed + ranP2speed;
                           totalP2keys = totalP2keys + ranP2pick;
                           firstPlayer = 1;
                           myhandler2.sendEmptyMessage(0);
                       }


                   } else {

                       break;
                   }


               } while (totalSpeed < 90 && totalP2keys < 50 && totalP1keys < 50);


               if ((totalSpeed < 90 && totalP1keys > 50 && totalP2keys < 50) || (totalSpeed > 90 && totalP1keys > totalP2keys)) {
                   myhandler3.sendEmptyMessage(0);

                   System.out.println("PLAYER 1 WINS!!!!");
                   System.out.println("Total Player 1 keys placed is" + totalP1keys + "and Total time elapsed is" + totalSpeed);
                   System.out.println("Total Player 2 keys placed is" + totalP2keys + "and Total time elapsed is" + totalSpeed);



               }

               else {

                   myhandler4.sendEmptyMessage(0);

                   System.out.println("PLAYER 2 WINS!!!!!!");
                   System.out.println("Total Player 2 keys placed is" + totalP2keys + "and Total time elapsed is" + totalSpeed);
                   System.out.println("Total Player 1 keys placed is" + totalP1keys + "and Total time elapsed is" + totalSpeed);


               }



           }
       };
       Thread kirwasThread = new Thread(myappRunnable);
         kirwasThread.start();



        }

     // this Button resets the application
    public void rstapp (View view) {
        TextView playWinnr = (TextView) findViewById(R.id.playWinnr);
        playWinnr.setText("Winner is...");
        ProgressBar p1progressBar = (ProgressBar) findViewById(R.id.p1progressBar);
        ProgressBar p2progressBar = (ProgressBar) findViewById(R.id.p2progressBar);
        ProgressBar totalkeyprogressBar = (ProgressBar) findViewById(R.id.totalkeyprogressBar);
        ProgressBar keyremprogressBar3 = (ProgressBar) findViewById(R.id.keyremprogressBar3);
        TextView clktextview = (TextView) findViewById(R.id.clktextview);
        clktextview.setText("Simulation clock" );
        p1progressBar.setProgress(0);
        keyremprogressBar3.setProgress(0);
        p2progressBar.setProgress(0);
        totalkeyprogressBar.setProgress(0);



    }

}








