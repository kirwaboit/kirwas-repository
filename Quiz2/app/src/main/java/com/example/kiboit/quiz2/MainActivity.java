package com.example.kiboit.quiz2;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements MainCallbacks{


    FragmentTransaction ft;
    FragmentLeft leftFragment;
    FragmentRight rightFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


// create a new LEFT fragment - show it
        ft = getFragmentManager().beginTransaction();
        leftFragment = FragmentLeft.newInstance("first-blue");
        ft.replace(R.id.main_holder_blue, leftFragment);
        ft.commit();

// create a new RIGHT fragment - show it
        ft = getFragmentManager().beginTransaction();
        rightFragment = RightRed.newInstance("first-red");
        ft.replace(R.id.main_holder_red, rightFragment);
        ft.commit();
    }

}

//creating SQL database

    TextView txtMsg = (TextView) findViewById(R.id.txtMsg);
    // path to the external SD card (something like: /storage/sdcard/...)
// String storagePath = Environment.getExternalStorageDirectory().getPath();
// path to internal memory file system (data/data/cis470.matos.databases)
    File storagePath = getApplication().getFilesDir(); String myDbPath = storagePath + "/" +
            "myfriends"; txtMsg.setText("DB Path: " + myDbPath);
    try {
        db = SQLiteDatabase.openDatabase(myDbPath, null,
                SQLiteDatabase.CREATE_IF_NECESSARY);
// here you do something with your database ... db.close();
        txtMsg.append("\nAll done!");
    } catch (SQLiteException e) {
        txtMsg.append("\nERROR " + e.getMessage());


  //populating database table


        create table tblAMIGO (
                recID integer PRIMARY KEY autoincrement, name text,
                phone text );


        insert into tblAMIGO(name, phone) values ('AAA', '555-1111' );


        db.execSQL("create table tblAMIGO ("
        + " recID integer PRIMARY KEY autoincrement, "
        + " name  text, "
        + " phone text );  "    );
        db.execSQL( "insert into tblAMIGO(name, phone) values ('AAA', '555-1111');" ); db.execSQL( "insert
        into tblAMIGO(name, phone) values ('BBB', '555-2222');" ); db.execSQL( "insert into tblAMIGO(name,
        phone) values ('CCC', '555-3333');"  );





    }
