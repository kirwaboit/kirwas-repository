package com.example.amuka.uitesting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.os.AsyncTask;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;


public class Main2Activity extends AppCompatActivity {
    EditText editIp;
    ImageButton kirnot,cyborgnot,nancynot,richnot,velmanot,sharnot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        editIp = (EditText)findViewById(R.id.ipaddrs);
        kirnot = (ImageButton)findViewById(R.id.imageButton2);
        cyborgnot = (ImageButton)findViewById(R.id.imageButton3);
        nancynot = (ImageButton)findViewById(R.id.imageButton4);
        richnot = (ImageButton)findViewById(R.id.imageButton);
        velmanot = (ImageButton)findViewById(R.id.imageButton6);
        sharnot = (ImageButton)findViewById(R.id.imageButton5);

        kirnot.setOnClickListener(kirbuttonlistener);
        cyborgnot.setOnClickListener(cyborgbuttonlistener);
        nancynot.setOnClickListener(nancybuttonlistener);
        richnot.setOnClickListener(richbuttonlistener);
        velmanot.setOnClickListener(velbuttonlistener);
        sharnot.setOnClickListener(sharbuttonlistener);

    }

    View.OnClickListener kirbuttonlistener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            String onoff;
            if(v==kirnot){
                onoff="1";
            }else{
                onoff="0";
            }

            kirnot.setEnabled(false);
            cyborgnot.setEnabled(false);
            nancynot.setEnabled(false);
            richnot.setEnabled(false);
            velmanot.setEnabled(false);
            sharnot.setEnabled(false);

            String serverIP = editIp.getText().toString()+":80";

            TaskEsp taskEsp = new TaskEsp(serverIP);
            taskEsp.execute(onoff);

        }
    };

    View.OnClickListener cyborgbuttonlistener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            String onoff;
            if(v==cyborgnot){
                onoff="1";
            }else{
                onoff="0";
            }

            kirnot.setEnabled(false);
            cyborgnot.setEnabled(false);
            nancynot.setEnabled(false);
            richnot.setEnabled(false);
            velmanot.setEnabled(false);
            sharnot.setEnabled(false);

            String serverIP = editIp.getText().toString()+":80";

            TaskEsp taskEsp = new TaskEsp(serverIP);
            taskEsp.execute(onoff);

        }
    }; View.OnClickListener nancybuttonlistener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            String onoff;
            if(v==nancynot){
                onoff="1";
            }else{
                onoff="0";
            }

            kirnot.setEnabled(false);
            cyborgnot.setEnabled(false);
            nancynot.setEnabled(false);
            richnot.setEnabled(false);
            velmanot.setEnabled(false);
            sharnot.setEnabled(false);

            String serverIP = editIp.getText().toString()+":80";

            TaskEsp taskEsp = new TaskEsp(serverIP);
            taskEsp.execute(onoff);

        }
    };
    View.OnClickListener richbuttonlistener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            String onoff;
            if(v==richnot){
                onoff="1";
            }else{
                onoff="0";
            }

            kirnot.setEnabled(false);
            cyborgnot.setEnabled(false);
            nancynot.setEnabled(false);
            richnot.setEnabled(false);
            velmanot.setEnabled(false);
            sharnot.setEnabled(false);

            String serverIP = editIp.getText().toString()+":80";

            TaskEsp taskEsp = new TaskEsp(serverIP);
            taskEsp.execute(onoff);

        }
    };
    View.OnClickListener velbuttonlistener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            String onoff;
            if(v==velmanot){
                onoff="1";
            }else{
                onoff="0";
            }

            kirnot.setEnabled(false);
            cyborgnot.setEnabled(false);
            nancynot.setEnabled(false);
            richnot.setEnabled(false);
            velmanot.setEnabled(false);
            sharnot.setEnabled(false);

            String serverIP = editIp.getText().toString()+":80";

            TaskEsp taskEsp = new TaskEsp(serverIP);
            taskEsp.execute(onoff);

        }
    };
    View.OnClickListener sharbuttonlistener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            String onoff;
            if(v==sharnot){
                onoff="1";
            }else{
                onoff="0";
            }

            kirnot.setEnabled(false);
            cyborgnot.setEnabled(false);
            nancynot.setEnabled(false);
            richnot.setEnabled(false);
            velmanot.setEnabled(false);
            sharnot.setEnabled(false);

            String serverIP = editIp.getText().toString()+":80";

            TaskEsp taskEsp = new TaskEsp(serverIP);
            taskEsp.execute(onoff);

        }
    };

    private class TaskEsp extends AsyncTask<String, Void, String> {

        String server;

        TaskEsp(String server){
            this.server = server;
        }

        @Override
        protected String doInBackground(String... params) {

            String val = params[0];
            final String p = "http://"+server+"?led="+val;

            runOnUiThread(new Runnable(){
                @Override
                public void run() {
                  //  textInfo1.setText(p);
                }
            });

            String serverResponse = "";
            HttpClient httpclient = new DefaultHttpClient();
            try {
                HttpGet httpGet = new HttpGet();
                httpGet.setURI(new URI(p));
                HttpResponse httpResponse = httpclient.execute(httpGet);

                InputStream inputStream = null;
                inputStream = httpResponse.getEntity().getContent();
                BufferedReader bufferedReader =
                        new BufferedReader(new InputStreamReader(inputStream));
                serverResponse = bufferedReader.readLine();

                inputStream.close();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                serverResponse = e.getMessage();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                serverResponse = e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                serverResponse = e.getMessage();
            }

            return serverResponse;
        }

        @Override
        protected void onPostExecute(String s) {
          //  textInfo2.setText(s);
            kirnot.setEnabled(true);
            cyborgnot.setEnabled(true);
            nancynot.setEnabled(true);
            richnot.setEnabled(true);
            velmanot.setEnabled(true);
            sharnot.setEnabled(true);
        }
    }



}
